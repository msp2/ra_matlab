function table = read_selection_table(fpath)
%
%  Usage:
%       read_selection_table
%
%  Inputs:
%       fpath - full path of selection table (char)
%  Outputs:
%       table - selection table (cell)
%
%  Description:
%       Reads specified Raven seletion table and returns a cell vector with
%       column headings and cell matrix with contents.
%
% History
%   msp2  Apr-11-2017   Initial


%---
% Initialize outputs to avoid error if function fails to complete execution
%---
table = {};

%---
% If no fpath, prompt user for selection table
%---
if ~exist('fpath', 'var')
    [fn, pth] = uigetfile('*.txt', 'Select Raven Selection Table');
    
    % Stop execution if user cancels uigetfile
    if isequal(fn, 0)
        return;
    end
    
    % Combine fn and pth to make fpath
    fpath = fullfile(pth, fn);    
end

    
%---
% Read selection table, output character matrix
%---
mat = caseread(fpath);


%---
% Figure out how many columns the selection table has
%---

% Split headers from selection table body
header_mat = mat(1, :);

% Remove extra columns at end
header_mat = strtrim(header_mat);
% % tab = sprintf('\t');
% % header_mat = strrep(header_mat, [tab, tab ], '');
% % header_mat = strrep(header_mat, [tab, tab], '');

% Separate headers into cell vector
headers = strsplit(header_mat, '\t');

% Count headers
num_col = length(headers);


%---
% Convert character matrix to cell array
%---

% Initializations
[num_row , ~] = size(mat);
table = cell(num_row, num_col);  %pre-allocate cell matrix to speed execution

% for each line
for i = 1:num_row
    
    % Copy current row in selectoin table
    line = mat(i, :);
    
    % Remove extra columns at end
    line = strtrim(line);

    % if empty line encountered, discard rest of table
    if isempty(line)
      table(i:end, :) = [];
      break;
    end

    % parse line into fields
    lineC = strsplit(line, '\t', 'CollapseDelimiters', false);
    table(i, 1:num_col) = lineC(1, 1:num_col);
end 
