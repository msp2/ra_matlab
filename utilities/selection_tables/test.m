% script test

%read selection table
fpath = 'E:\Bitbucket_GIT\RA_MATLAB\utilities\read_selection_table\data\input\AMAR358_A3_8kHz_150724_205829_subset_HandBrowse_bje1_soundStream.ACOUSTAT.selections.txt';
table = read_selection_table(fpath);

% Get Begin Time values
begin_time = get_column(table, 'Begin Time (s)');

% sort selection table by MSNR [dB]
header = 'MSNR [dB]';
numeric_sort = true;
direction = 'descend';
sorted_table = sort_by_column(table, header, numeric_sort, direction);

%-----------------------------------------------------------------------

%read selection table
fpath = 'E:\Bitbucket_GIT\RA_MATLAB\utilities\read_selection_table\data\input\MD04_002K_M12_multi_20160225-20160804_Fin_25%_data_selection_tables_consolidated_FOR_XBAT+FileOffset.txt';
table = read_selection_table(fpath);

% keep only the selections where "Verification 2" is "y" or "v"
header = 'Verification 2';
pattern = {'y', 'v'}; 
filtered_table = filter_by_column(table, header, pattern);