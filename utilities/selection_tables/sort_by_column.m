function [sorted_table, sorted_values] = sort_by_column(table, header, numeric_sort, direction)
%
%  Usage:
%       sort_by_column
%
%  Inputs:
%       in_table - selection table, including headers (cell array of char vectors)
%       header - header for column of interest (char)
%       numeric_sort - numeric sort (logical)
%       direction - 'ascend' or 'descind' (char)
%  Outputs:
%       out_table - selection table sorted as specified
%
%  Description:
%       Sorts rows in a selection table by specified column.
%       The default sort is alphabetic, since Raven selection tables are
%       text files. Setting "numeric_sort" to "true" insures that numbers are
%       sorted using a numeric sort.
%
% History
%   msp2  Apr-13-2017   Initial

% Stop execution if no data to sort
if size(table, 1) < 2
    sorted_table = table;
    sorted_values = {};
    return;
end

% Get specified column
values = get_column(table, header);

% Convert alphabetic values to numeric values, if specified
if numeric_sort
    values_to_sort = str2double(values);
else
    values_to_sort = values;
end    

% Sort values
[~, idx] = sort(values_to_sort, direction);
sorted_values = values(idx);

% Separate headers from body of tab
headers = table(1, :);
body = table(2:end, :);

% Sort body of selection table
sorted_body = body(idx, :);

% Prepend headers to body of selection table
sorted_table = [headers; sorted_body];