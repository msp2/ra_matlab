function values = get_column(table, header)
%
%  Usage:
%       get_column
%
%  Inputs:
%       table - selection table, including headers (cell array of char vectors)
%       header - header for column of interest (char)
%  Outputs:
%       values - column values (cell array of char vectors)
%
%  Description:
%       Returns the values in one column of a Raven selection table, as
%       specified by column header.
%
% History
%   msp2  Apr-13-2017   Initial

%---
% Find index of column with specified header
%---
headers = table(1, :);
idx = ismember(headers, header);

%---
% Copy values in specified column
%---
body = table(2:end, :);
values = body(:, idx);