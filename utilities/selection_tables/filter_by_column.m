function filtered_table = filter_by_column(table, header, pattern)
%
%  Usage:
%       filtered_table
%
%  Inputs:
%       table - selection table, including headers (cell array of char vectors)
%       header - header for column of interest (char)
%       pattern - string to filter by (cell array of char vectors)
%  Outputs:
%       filtered_table - filtered selection table (cell array of char vectors)
%
%  Description:
%       Outputs only those selections that have the annotation column
%       specified by "header" equal to one of the strings specified in
%       "pattern".
%
% History
%   msp2  Apr-13-2017   Initial

% Stop execution if no data to filter
if size(table, 1) < 2
    filtered_table = table;
    return;
end

% Get specified column
values = get_column(table, header);

% Find indices of selections that match pattern
idx = ismember(values, pattern);

% Separate headers from body of tab
headers = table(1, :);
body = table(2:end, :);

% Filter body of selection table
filtered_body = body(idx, :);

% Prepend headers to body of selection table
filtered_table = [headers; filtered_body];