function list = file_tree(inpath, filespec)
%
%  Usage:
%   file_tree
%
%  Inputs:
%       inpath - path of file tree root (char)
%       filespec - file filter criterion (char)
%                  (eg, '*.*', '*.aif', '*.wav')
%  Outputs:
%       list - list of files meeting search criteria (cell)
%
%  Description:
%       Reads specified Raven seletion table and returns a cell vector with
%       column headings and cell matrix with contents.
%
% History
%   msp2  Apr-12-2017   Initial

% Initializations
list = {};

% Warn user if 'inpath' is valid
t = sprintf('\nSpecified "inpath" is not valid:\n   %s', inpath);
assert(isdir(inpath), t)

% create waitbar
h = waitbar(0, 'Finding sound files. Please wait.');

% Find list of files and write to data folder
inpathFull = fullfile(inpath, filespec);
cmd = sprintf('dir "%s" /s/b/a-d | sort', inpathFull);
[~, results] = dos(cmd);


% Parse results into cell vector of file paths
list = strsplit(results(1:end-1), char(10))';

% Delete waitbar
delete(h)