%  Usage:
%   split_channels_wrapper
%
%  Description:
%       Calls split_channels
%
% History
%   msp2  Apr-12-2017   Initial

% Navigate to root of input sound library
inpath = uigetdir(pwd, 'Select root directory of input sound library');
if isequal(inpath, 0)
    return;
end

% Navigate to root of output sound library
outpath = uigetdir(pwd, 'Select root directory of output sound library');
if isequal(outpath, 0)
    return;
end

% Prompt user for sound file extension
prompt = 'Enter sound file extension (eg ".aif", ".wav"';
dlg_title = '';
num_lines = 1;
defaultans = {'.aif'};
ext = inputdlg(prompt, dlg_title, num_lines, defaultans);
if isempty(ext)
    return;
end
ext = ext{1};
filter = sprintf('*%s', ext);

% Prompt user for channels to filter
prompt = 'Enter channels to keep (eg, "3 7 10")';
dlg_title = '';
num_lines = 1;
defaultans = {'1'};
channels = inputdlg(prompt, dlg_title, num_lines, defaultans);
if isempty(channels)
    return;
end
channels = channels{1};

% Test if user's channel designation is valid
t1 = 'is not a valid channel specification.';
t2 = 'Please enter channel numbers separated by spaces.';
t = sprintf('\n\nWARNING\n"%s" %s\n%s', channels, t1, t2);
assert(isempty(regexp(channels, '[^\d\s]', 'ONCE')), t)

% Find list of sound files in sound library
inlist = file_tree(inpath, filter);
len = length(inlist);

% Make list of full output paths
outlist = strrep(inlist, inpath, outpath);


%---
% Split channels
%---

% Create waitbar
h = waitbar(0, 'Stripping sound files. Please wait.');

% Split channels from sound files, one by one
for i = 1:len
    waitbar(i/len, h)
    split_channels(inlist{i}, outlist{i}, channels)
end

% Finalize waitbar
waitbar(1, h, 'Splitting complete.')
