function split_channels(inpathFull, outpathFull, channels)
%
%  Usage:
%   split_channels
%
%  Inputs:
%       inpathFull - full path of sound file (char)
%       channels - row vector of channels to include in output (char)
%  Outputs:
%
%  Description:
%       Strips all but the specified channels from specified sound file,
%       and write to new sound file in a subdirectory named "split".
%
%  Must have SoX installed for this to work
%       https://sourceforge.net/projects/sox/
%
% History
%   msp2  Apr-12-2017   Initial

% Check if SoX is installed
[status, ~] = dos('sox -help');
if status
    fprintf(2,'\n\nWARNING:\n\nSoX must be installed for split_channels to work.\n\n');
    fprintf(2, 'Download SoX installer from https://sourceforge.net/projects/sox/.\n');
    return;
end

% Make output directory, if needed
outpath = fileparts(outpathFull);
if ~isdir(outpath)
    mkdir(outpath)
end

%--
% Write stripped file
cmd = sprintf('sox "%s" "%s" remix %s', inpathFull, outpathFull, channels);
[status, results] = dos(cmd);
if status
    fprintf(2, '\n\n');
    fprintf(2, '%s\n', results);
end